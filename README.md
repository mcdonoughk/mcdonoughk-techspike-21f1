# Tech Spike: TypeScript, SQLite, and JSON Web Tokens (JWTs)
**Goal:** Create familiarity with the technologies currently in use on the backend of the project. Demonstrate that each core component, TypeScript, SQLite, and JSON Web Tokens are understood and can be utilized effectively.

## Key Technologies
* **[Node.js](https://nodejs.org/dist/latest-v14.x/docs/api/) (14.17.6):** underlying software; runs it all
* **[TypeScript](https://www.typescriptlang.org/) (4.4.3):** JavaScript with types
* **[Express](https://expressjs.com/en/4x/api.html) (4.17.1):** web framework (handle requests, middleware, etc.)
* **[SQLite3](https://github.com/mapbox/node-sqlite3/wiki/API) (5.0.2):** library to interface with SQLite databases
* **[Sequelize](https://sequelize.org/) (6.6.5):** library to interact with SQLite via promises (ORM)
* **[nJwt](https://github.com/jwtk/njwt) (1.1.0):** library to create/sign and verify [JSON Web Tokens (JWTs)](https://jwt.io/)
* **[secure-random](https://github.com/jprichardson/secure-random) (1.1.2):** library to randomly generate a signing token for JWTs.

## Additional Technologies
* **[bcrypt.js](https://github.com/dcodeIO/bcrypt.js) (2.4.3):** Password hashing and verification (not central to this spike, but good to have)

## Installation
> **Note:** There is no user interface for this program; it is a REST API to be interacted with by a user interface (not created) or, more specifically for this tech spike, an API client such as [Postman](https://www.postman.com/).

> **Note:** These instructions assume you have installed [Node.js](https://nodejs.org/) and [Git](https://git-scm.com/).

Clone and enter the repository (cloning via ssh is recommended).
```bash
git clone git@gitlab.com:mcdonoughk/mcdonoughk-techspike-21f1.git
cd mcdonoughk-techspike-21f1.git
```

Install the dependencies.
```bash
npm install
```

### Running
```bash
npm run build
npm start
```

Once the application has finished starting, a link should be displayed in the console.

### Development
If you intend on making changes, running the `start-dev` option will reflect changes made in the `src` directory automatically upon saving.
```bash
npm run start-dev
```

Once the application has finished starting, a link should be displayed in the console. The program will automatically recompile and restart when changes are made.

## Endpoints
### `GET /user/{username}`
Gets a user by the provided username. Will return the id, username, display name, when the account was created, and when the account was last updated.

**Example:**

`GET /user/admin`

Response:

```json
{
    "id": "65efafcd-2453-4d8f-a3e8-654d2756d461",
    "username": "admin",
    "displayName": "Administrator",
    "createdAt": "2021-09-28T17:03:46.518Z",
    "updatedAt": "2021-09-28T17:03:46.518Z"
}
```

### `POST /user`
Creates a new user. Will return the new id, username, display name, when the account was created, and when the account was updated with HTTP status 201 Created, or 409 Conflict if there is a problem creating the account (provides a reason).

**Example:**

`POST /user`

Body:

```json
{
    "username": "student",
    "displayName": "John Doe",
    "password": "password"
}
```

Response:

```json
{
    "id": "9a78a0f8-fe62-4a9d-8cf1-2017b9cc6afb",
    "username": "student",
    "displayName": "John Doe",
    "createdAt": "2021-10-04T01:51:50.681Z",
    "updatedAt": "2021-10-04T01:51:50.681Z"
}
```

### `POST /session`
Creates a new session. Returns 200 OK with a JSON Web Token (JWT) or 401 Unauthorized.

**Example:**

`POST /session`

Body:

```json
{
    "username": "student",
    "password": "password"
}
```

Response:

```json
{
    "token": "{json web token here, very long}"
}
```

### `GET /session/test`
Endpoint that requires an JSON Web Token to access. Returns 200 OK and a message with the user's display name or 401 Unauthorized.

**Example:**

`GET /session/test`

Authorization Header: `Bearer {json web token}`

Response:

```json
{
    "message": "You are authorized! Hello, John Doe!"
}
```
