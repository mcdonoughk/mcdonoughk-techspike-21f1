import express from "express";
import { Database } from "./database";

import userController from "./controllers/user";
import sessionController from "./controllers/session";

const app = express();
const database: Database = new Database();

app.use("/user", userController);
app.use("/session", sessionController);

database.initialize().then(() => {
  app.listen(8080, () => console.log(`Listening at http://localhost:8080`));
});