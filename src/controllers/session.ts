import express from "express";
import { Database } from "../database";
import { ConfigModel } from "../models/config";
import bcryptjs from "bcryptjs";
import njwt from "njwt";
import { removeListener } from "process";
import jwt from "express-jwt";
import { UserModel } from "../models/user";

const router = express.Router();

const database: Database = new Database();

let signingKey: string;

database.initialize().then(async () => {
  signingKey = (await database.getConfigValue("SigningKey")).value;

  router.use(express.json());

  router.post("/", async (req, res) => {
    if (req.body.username === undefined || req.body.password === undefined) {
      res.status(400).json({message: "Invalid credentials, expected username & password."});
      return;
    }
    let username = req.body.username;
    let password = req.body.password;

    let search = await database.getUser(username, true);
    if (search !== null) {
      let validPassword = await bcryptjs.compare(password, search.password);
      if (validPassword) {
        let claims = {
          iss: "https://localhost:8080",
          sub: search.id,
          userType: search.userType
        }
        let token = njwt.create(claims, Buffer.from(signingKey));
        res.json({token: token.compact()});
        return;
      }
    }
    res.status(401).json({message: "Invalid credentials."});
  });

  router.get("/test", validateJWT, async (req, res) => {
    let user: UserModel = <UserModel>req.user;
    res.json({message: `You are authorized! Hello, ${user.displayName}!`});
  });
});

export {};
export default router;

export async function validateJWT(req: express.Request, res: express.Response, next: express.NextFunction) {
  let authorization = req.headers.authorization;
  if (authorization === undefined) {
    // No authorization header present
    res.status(401).json({message: "Unauthorized"});
    return;
  } else if (authorization.split(" ")[0] !== "Bearer") {
    // Incorrect format for authorization header
    res.status(401).json({message: "Unauthorized"});
    return;
  }
  let token = authorization.split(" ")[1];
  let jwt;
  try {
    jwt = njwt.verify(token, Buffer.from(signingKey));
  } catch (error) {
    // JWT is invalid
    res.status(401).json({message: "Unauthorized"});
    return;
  }
  let user = await database.getUserById(jwt.body.toJSON().sub.toString(), true);
  req.user = user;
  next();
}