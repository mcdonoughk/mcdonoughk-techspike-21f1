import express from "express";
import { Database } from "../database";
import { UserType } from "../models/user";

const router = express.Router();

const database: Database = new Database();

database.initialize().then(() => {
  router.use(express.json());
  router.post('/', async (req, res) => {
    let username = req.body.username;
    let displayName = req.body.displayName;
    let password = req.body.password;
    let result = await database.createUser(username, displayName, password, UserType.Team);
    if (result === false) {
      res.status(409).json({message: "Username already taken."});
    } else {
      res.status(201).json(await database.getUser(username));
    }
  });
  router.get('/:username', async (req, res) => {
    let user = await database.getUser(req.params.username);
    if (user !== null) {
      res.status(200).json(user);
    } else {
      res.status(404).json({message: "User not found."});
    }
  });
  router.put('/:username', async (req, res) => {
    
  });
})

export {};
export default router;