import { BuildOptions, DataTypes, Model, Sequelize } from "sequelize";

export enum UserType {
  Admin = "admin",
  Judge = "judge",
  Team = "team"
};

export interface UserAttributes {
  id: string,
  displayName: string,
  username: string,
  password: string,
  userType: UserType,
  createdAt?: Date,
  updatedAt?: Date
};

export interface UserModel extends Model<UserAttributes>, UserAttributes {};

export class User extends Model<UserModel, UserAttributes> {};

export type UserStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): UserModel
};

export function userFactory(sequelize: Sequelize): UserStatic {
  return <UserStatic>sequelize.define("User", {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4
    },
    displayName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    username: {
      type: DataTypes.STRING(16),
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    userType: {
      type: DataTypes.ENUM("admin", "judge", "team"),
      allowNull: false
    }
  });
}