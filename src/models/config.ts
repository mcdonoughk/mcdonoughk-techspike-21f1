import { BuildOptions, DataTypes, Model, Sequelize } from "sequelize";

export interface ConfigAttributes {
  key: string,
  value: string
};

export interface ConfigModel extends Model<ConfigAttributes>, ConfigAttributes {};

export class Config extends Model<ConfigModel, ConfigAttributes> {};

export type ConfigStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): ConfigModel
};

export function configFactory(sequelize: Sequelize): ConfigStatic {
  return <ConfigStatic>sequelize.define("ConfigurationValue", {
    key: {
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });
}