import { genSalt, hash } from "bcryptjs";
import { randomUUID } from "crypto";
import { Sequelize } from "sequelize";
import { User, userFactory, UserStatic, UserType } from "./models/user";
import njwt from "njwt";
import secureRandom from "secure-random";
import { Config, configFactory, ConfigStatic } from "./models/config";

export class Database {
  database: Sequelize;
  User: UserStatic;
  Config: ConfigStatic;

  public async initialize() {
    this.database = new Sequelize({
      dialect: "sqlite",
      storage: "data/database.db"
    });

    this.User = userFactory(this.database);
    this.Config = configFactory(this.database);

    try {
      await this.database.authenticate();
      await this.database.sync();
      await this.createAdminUser();
      await this.createSigningKey();
    } catch (error) {
      throw error;
    }
  }

  public async getUser(username: string, complete?: boolean) {
    return await this.User.findOne({
      where: {
        username: username
      },
      attributes: complete === true ?
                  ['id', 'username', 'displayName', 'password', 'userType', 'createdAt', 'updatedAt'] :
                  ['id', 'username', 'displayName', 'createdAt', 'updatedAt']
    });
  }

  public async getUserById(id: string, complete?: boolean) {
    return await this.User.findOne({
      where: {
        id: id
      },
      attributes: complete === true ?
                  ['id', 'username', 'displayName', 'password', 'userType', 'createdAt', 'updatedAt'] :
                  ['id', 'username', 'displayName', 'createdAt', 'updatedAt']
    });
  }

  public async createUser(username: string, displayName: string, password: string, userType: UserType) {
    if (await this.getUser(username) !== null) return false;
    let newUser = await this.User.create({
      id: randomUUID(),
      username: username,
      displayName: displayName,
      password: await hash(password, await genSalt()),
      userType: userType
    });

    await newUser.save();
    return newUser;
  }

  public async getConfigValue(key: string) {
    return await this.Config.findOne({
      where: {
        key: key
      },
      attributes: ['key', 'value', 'createdAt', 'updatedAt']
    })
  }

  private async createAdminUser() {
    await this.createUser("admin", "Administrator", "admin", UserType.Admin);
  }

  private async createSigningKey() {
    if (await this.getConfigValue("SigningKey") !== null) return;
    let signingKey = secureRandom(256, {type: 'Buffer'});
    let newConfigValue = await this.Config.create({
      key: "SigningKey",
      value: signingKey.toString('base64')
    });
    await newConfigValue.save();
  }
}